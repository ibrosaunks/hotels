<?php
include ("initialize.php");

$mysqli = new mysqli("localhost", "root", "", "hotels");


$lineseparator = "\n";
$fieldseparator = "|";
$file = new SplFileObject("Hotel_All_Active 09-07-13.txt");

$query = "CREATE TABLE IF NOT EXISTS international_info (";
$query .=  "HotelID int(6) NOT NULL,";
$query .=  "Name varchar(70) NOT NULL,";
$query .=  "AirportCode char(3) DEFAULT NULL,";
$query .=  "Address1 varchar(50) DEFAULT NULL,";
$query .=  "Address2 varchar(50) DEFAULT NULL,";
$query .=  "Address3 varchar(50) DEFAULT NULL,";
$query .=  "City varchar(50) DEFAULT NULL,";
$query .=  "StateProvince varchar(50) DEFAULT NULL,";
$query .=  "Country varchar(50) DEFAULT NULL,";
$query .=  "PostalCode varchar(15) DEFAULT NULL,";
$query .=  "Longitude float DEFAULT NULL,";
$query .=  "Latitude float DEFAULT NULL,";
$query .=  "LowRate int(8) DEFAULT NULL,";
$query .=  "HighRate int(8) DEFAULT NULL,";
$query .=  "MarketingLevel int(4) DEFAULT NULL,";
$query .=  "Confidence int(4) DEFAULT NULL,";
$query .=  "HotelModified datetime DEFAULT NULL,";
$query .=  "PropertyType char(5) DEFAULT NULL,";
$query .=  "TimeZone varchar(80) DEFAULT NULL,";
$query .=  "GMTOffset char(6) DEFAULT NULL,";
$query .=  "YearPropertyOpened varchar(256) DEFAULT NULL,";
$query .=  "YearPropertyRenovated varchar(256) DEFAULT NULL,";
$query .=  "NativeCurrency char(3) DEFAULT NULL,";
$query .=  "NumberOfRooms int(4) DEFAULT NULL,";
$query .=  "NumberOfSuites int(4) DEFAULT NULL,";
$query .=  "NumberOfFloors int(4) DEFAULT NULL,";
$query .=  "CheckInTime varchar(10) DEFAULT NULL,";
$query .=  "CheckOutTime varchar(10) DEFAULT NULL,";
$query .=  "HasValetParking varchar(1) DEFAULT NULL,";
$query .=  "HasContinentalBreakfast varchar(1) DEFAULT NULL,";
$query .=  "HasInRoomMovies varchar(1) DEFAULT NULL,";
$query .=  "HasSauna varchar(1) DEFAULT NULL,";
$query .=  "HasWhirlpool varchar(1) DEFAULT NULL,";
$query .=  "HasVoiceMail varchar(1) DEFAULT NULL,";
$query .=  "Has24HourSecurity varchar(1) DEFAULT NULL,";
$query .=  "HasParkingGarage varchar(1) DEFAULT NULL,";
$query .=  "HasElectronicRoomKeys varchar(1) DEFAULT NULL,";
$query .=  "HasCoffeeTeaMaker varchar(1) DEFAULT NULL,";
$query .=  "HasSafe varchar(1) DEFAULT NULL,";
$query .=  "HasVideoCheckOut varchar(1) DEFAULT NULL,";
$query .=  "HasResrictedAccess varchar(1) DEFAULT NULL,";
$query .=  "HasInteriorRoomEntrance varchar(1) DEFAULT NULL,";
$query .=  "HasExteriorRoomEntrance varchar(1) DEFAULT NULL,";
$query .=  "HasCombination varchar(1) DEFAULT NULL,";
$query .=  "HasFitnessFacility varchar(1) DEFAULT NULL,";
$query .=  "HasGameRoom varchar(1) DEFAULT NULL,";
$query .=  "HasTennisCourt varchar(1) DEFAULT NULL,";
$query .=  "HasGolfCourse varchar(1) DEFAULT NULL,";
$query .=  "HasInHouseDining varchar(1) DEFAULT NULL,";
$query .=  "HasInHouseBar varchar(1) DEFAULT NULL,";
$query .=  "HasHandicapAccessible varchar(1) DEFAULT NULL,";
$query .=  "HasChildrenAllowed varchar(1) DEFAULT NULL,";
$query .=  "HasPetsAllowed varchar(1) DEFAULT NULL,";
$query .=  "HasTVInRoom varchar(1) DEFAULT NULL,";
$query .=  "HasDataPorts varchar(1) DEFAULT NULL,";
$query .=  "HasMeetingRooms varchar(1) DEFAULT NULL,";
$query .=  "HasBusinessCenter varchar(1) DEFAULT NULL,";
$query .=  "HasDryCleaning varchar(1) DEFAULT NULL,";
$query .=  "HasIndoorPool varchar(1) DEFAULT NULL,";
$query .=  "HasOutdoorPool varchar(1) DEFAULT NULL,";
$query .=  "HasNonSmokingRooms varchar(1) DEFAULT NULL,";
$query .=  "HasAirportTransportation varchar(1) DEFAULT NULL,";
$query .=  "HasAirConditioning varchar(1) DEFAULT NULL,";
$query .=  "HasClothingIron varchar(1) DEFAULT NULL,";
$query .=  "HasWakeUpService varchar(1) DEFAULT NULL,";
$query .=  "HasMiniBarInRoom varchar(1) DEFAULT NULL,";
$query .=  "HasRoomService varchar(1) DEFAULT NULL,";
$query .=  "HasHairDryer varchar(1) DEFAULT NULL,";
$query .=  "HascarRentDesk varchar(1) DEFAULT NULL,";
$query .=  "HasFamilyRooms varchar(1) DEFAULT NULL,";
$query .= "HasKitchen varchar(1) DEFAULT NULL,";
$query .=  "HasMap bit(1) DEFAULT NULL,";
$query .=  "PropertyDescription text,";
$query .=  "GDSChainCode varchar(10) DEFAULT NULL,";
$query .=  "GDSChainCodeName varchar(70) DEFAULT NULL,";
$query .=  "DestinationId varchar(60) DEFAULT NULL,";
$query .=  "DrivingDirections text,";
$query .=  "NearbyAttractions text,";
$query .=  "PRIMARY KEY (HotelID)";
$query .= ") ENGINE=InnoDB DEFAULT CHARSET=latin1;";

$result = $mysqli->query($query);
echo $mysqli->error;
$linearray = array();
while(!$file->eof()){
	/* $buffer = fgets($file_open, 4096);
	$lines[] = $buffer;
	
	var_dump($lines); 
	*/
	$line = $file->current();
	$line = trim($line," \t");
	$line = str_replace("\r","",$line);
	$line = str_replace("'", "\'", $line);
	$linearray = explode($fieldseparator, $line);
	$line = "";
	$i = 0;
	foreach($linearray as $value){
		
		if(strlen($value) == 0 || $value == "" || $value == " " || $value == "  " || $value == "   "){
			$value = null;
		} 
		
			if($i != 77 && ($value != null)){
				
				$line .= "'". $value . "', ";
			}
			elseif($value == null)
				$line .= 'NULL' . ", " ;
			
			else
				$line .= "'" . $value . "'";
		
		
		
		$i++;	
	}
	
	$query = "insert into international_info values(".$line.");";
	$result = $mysqli->query($query);
	if(empty($mysqli->error))
		echo "done <br />";
	else 
		echo $mysqli->error . "<br />";
	$file->next();
}

$mysqli->close();

 
?>